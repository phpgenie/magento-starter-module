<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    PHPGenie
 * @package     PHPGenie_StarterModule
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @generator   http://www.mgt-commerce.com/kickstarter/ Mgt Kickstarter
 */

/*
 * The name of the block is MyBlock, which is within the PHPGenie Package, 
 * and the StarterModule module
 * 
 * This Block will have the shortname:
 * phpgenie_startermodule/myblock
 * 
 * This can be used within another design file:
 * $this->getLayout()->createBlock('phpgenie_startermodule/myblock');
 * 
 * Or within a WYSIWYG Editor:
 * {{block type="phpgenie_startermodule/myblock" }}
 */

class PHPGenie_StarterModule_Block_MyBlock extends Mage_Core_Block_Template {
    /*
     * The construct method is run automatically when the block is instantiated.
     */

    public function __construct() {
        // It should run the contruct of it's parent, just to make sure everything works as expected,
        // which in this case is Mage_Core_Block_Template
        parent::__construct();
        // This links the template file with block itself
        $this->setTemplate('phpgenie_startermodule/myblock.phtml');
    }

    /*
     * The template has immediate access to any of the public methods in this class
     */
    
    public function myPublicFunction(){
        
    }
    
    /*
     * Private and Protected Functions can be called within this class, but not outside of it.
     */
    
    protected function myPrivateFunction(){
        
    }
}
