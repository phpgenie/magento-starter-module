<?php
/**
 * This is an SQL installer.
 * It's file name and path is crucial, as this installs the requirements for version 1.0.0
 * into the resource defined within the config under config > global > resources
 * install-1.0.0.php
 *
 * To write an upgrade script, the code contained within can remain, but the filename must be:
 * upgrade-[Version From]-[Version To].php
 * upgrade-1.0.0-1.0.1.php
 */

$installer = $this;

// Starts the Setup Procedure
$installer->startSetup();

// Get table is used just in-case there is a table prefix,
// always thereby returing an appropriate string
$tableName = $installer->getTable('phpgenie_startermodule_table');

// The <<<SQLTEXT ... SQLTEXT declaration here allows you to plug in direct
// SQL queries in a safe environment. You can include PHP variables in the string
// by including the {} syntax.
$sql=<<<SQLTEXT
CREATE TABLE `{$tableName}` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR( 64 ) NOT NULL ,
  `created_at` DATETIME NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = InnoDB;
SQLTEXT;

// Runs the query $sql
$installer->run($sql);

// Ends the Setup Procedure
$installer->endSetup();