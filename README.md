# Magento Starter Module

This module is not meant to be installed to any Magento installation, but instead, is meant to be an educational tool by which to learn about Magento Modules.

#Index

Throughout this module, the main features are highlighted here, but you can find much more information within the files themselves alongside actual examples of functionality.

## Module Activation File

The first part of a module is the Module Activation file.

Files of Note:

- app/etc/modules/PHPGenie_StarterModule.xml

## The Config File

The config file is tied into pretty much every aspect of your module.
It's the first thing that Magento looks for when loading a module, and it tells Magento where all the relevant files can be found, and how the module should function.

Files of Note:

- app/code/local/PHPGenie/StarterModule/etc/config.xml

## Block

Blocks are the main parts of Modules.
They take a feature and render it to screen. They can be placed anywhere within your code, or even within WYSIWYG Editors through a very simple piece of code.

Files of Note:

- app/code/local/PHPGenie/StarterModule/etc/config.xml
- app/code/local/PHPGenie/StarterModule/Block/Myblock.php
- app/design/frontend/base/default/layout/phpgenie_startermodule.xml
- app/design/frontend/base/default/template/phpgenie_startermodule/myblock.phtml
- skin/frontend/base/default/css/phpgenie_startermodule/style.css
- skin/frontend/base/default/js/phpgenie_startermodule/*
- js/phpgenie_startermodule/*

## Helper

Helpers are unique pieces of code that you can utilise the site over.
This may be as simple as a calculation or a style for an object, but can be as complex as you like.
The helpers can be called from anywhere in the site.

Files of Note:

- app/code/local/PHPGenie/StarterModule/etc/config.xml
- app/code/local/PHPGenie/StarterModule/Helper/Data.php

## Controller

Controllers render a page to the screen and control the logic to allow it to function.
Similar in some ways to a block, where this differs is that a Controller would serve an entire screen, as opposed to a small chunk of it. This could extend to adding a collection of blocks, or something different entirely.

Files of Note:

- app/code/local/PHPGenie/StarterModule/etc/config.xml
- app/code/local/PHPGenie/StarterModule/controllers/IndexController.php
- app/code/local/PHPGenie/StarterModule/controllers/Adminhtml/IndexController.php

## Model & Resource

Models are the middle-man between database and block. They get and manage the data in preparation for displaying it to screen. This is much the same as other frameworks.

Files of Note:

- app/code/local/PHPGenie/StarterModule/etc/config.xml
- app/code/local/PHPGenie/StarterModule/Model/Mymodel.php
- app/code/local/PHPGenie/StarterModule/Model/Resource/Mymodel.php
- app/code/local/PHPGenie/StarterModule/Model/Resource/Mymodel/Collection.php

## An Event

Events are useful ways to hook into existing Magento core functionality to provide additional or adjusted functions.

- app/code/local/PHPGenie/StarterModule/etc/config.xml
- app/code/local/PHPGenie/StarterModule/Model/Observer.php