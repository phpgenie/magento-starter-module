<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    PHPGenie
 * @package     PHPGenie_StarterModule
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @generator   http://www.mgt-commerce.com/kickstarter/ Mgt Kickstarter
 */
/*
 * This is a helper and is able to provide methods to use across the site,
 * in other modules, or in your layout.
 * This is perfect for some utility calculations that you wish to make that 
 * could be applied site-wide.
 * 
 * For example, if you were to create a specific way of formatting a price, 
 * you could put all the various functions here and use them throughout the site.
 * 
 * The short name of this helper is: phpgenie_startermodule/data
 * This can be used in the code as follows: Mage::helper('phpgenie_startermodule/data');
 * This helper can access any public method within the class.
 */

class PHPGenie_StarterModule_Helper_Data extends Mage_Core_Helper_Abstract {
    /*
     * The helper has immediate access to any of the public methods in this class, 
     * wherever it is called
     */

    // Mage::helper('phpgenie_startermodule/data')->myPublicFunction();
    public function myPublicFunction() {
        
    }

    /*
     * Private and Protected Functions can be called within this class, but not outside of it.
     */

    protected function myPrivateFunction() {
        
    }

}
