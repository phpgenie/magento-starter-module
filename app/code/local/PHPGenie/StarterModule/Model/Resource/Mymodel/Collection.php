<?php
/**
 * The resource collection is designed to hold multiple resources.
 * This is ideal for returning full lists - like 'SELECT *' in SQL
 *
 * You will rarely use a resource by itself, but a collection can be returned with the following:
 * Mage::getModel('phpgenie_startermodule/mymodel')->getCollection();
 *
 * After getCollection, you can add 'where' conditions
 * ->addFieldToFilter('field_name', $field_value);
 */
class PHPGenie_StarterModule_Model_Resource_Mymodel_Collection extends Mage_Core_Model_Resource_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('phpgenie_startermodule/mymodel');
    }

}
