<?php
/**
 * This model is called Mymodel as an example.
 * The Mage_Core_Model_Abstract handles pretty much all the setup and configuration
 * for us, leaving only the construct function up to us.
 *
 * Models can be called as either Models or Singletons. 
 * The main difference between these two is that calling a Model will always
 * initialise a new instance, whereas Singletons will only initialise a new
 * instance if one doesn't already exist.
 *
 * This Model will have the shortname:
 * phpgenie_startermodule/mymodel
 * 
 * This can be used within another class:
 * Mage::getModel('phpgenie_startermodule/mymodel');
 * or
 * Mage::getSingleton('phpgenie_startermodule/mymodel');
 **/
class PHPGenie_StarterModule_Model_Mymodel extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        // This line defines the short name of this model
        $this->_init('phpgenie_startermodule/mymodel');
    }
}
