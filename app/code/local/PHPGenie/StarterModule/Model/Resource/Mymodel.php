<?php
/**
 * This is the model resource.
 * The resource is equivalent to a single database entry, and acts as a level of abstraction
 * that doesn't depend on a specific database technology.
 *
 * If you use the model to return a single item, this item will be a resource.
 * Mage::getModel('phpgenie_startermodule/mymodel')->load($id);
 **/
class PHPGenie_StarterModule_Model_Resource_Mymodel extends Mage_Core_Model_Resource_Abstract
{
    protected function _construct()
    {
        // This line defines the short name of this resource, and defines what the primary key is.
        $this->_init('phpgenie_startermodule/mymodel', 'id');
    }
}
