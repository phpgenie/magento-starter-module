<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    PHPGenie
 * @package     PHPGenie_StarterModule
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @generator   http://www.mgt-commerce.com/kickstarter/ Mgt Kickstarter
 */

/*
 * An Adminhtml controller works in the same way as a Frontend controller.
 * The controller follows a slightly different naming convention, in that Adminhtml
 * now sits between the Module Name and the Controller Name.
 * This is due to the folderstructure: 
 * PHPGenie/StarterModule/controllers/Adminhtml/IndexController.php
 */

class PHPGenie_StarterModule_Adminhtml_IndexController extends Mage_Core_Controller_Front_Action {
    /*
     * All actions are to follow the same naming convention: nameAction()
     * The following action is accessible via: /admin/startermodule/index/test
     * 
     * If you were to create a new page, under the name 'another' then the
     * function name would be anotherAction()
     */

    public function testAction() {
        // load the layout first
        $this->loadLayout();
        // create a block
        $myblock = $this->getLayout()->createBlock('phpgenie_startermodule/myblock');
        // add the block to the layout
        $this->_addContent($myblock);        
        // render the created layout to the screen
        $this->renderLayout();
    }

}
