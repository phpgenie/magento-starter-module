<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    PHPGenie
 * @package     PHPGenie_StarterModule
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @generator   http://www.mgt-commerce.com/kickstarter/ Mgt Kickstarter
 */
/*
 * This is a controller and controllers, like other frameworks, provide the logic
 * for the views.
 * 
 * You will see code such as $this->renderLayout(); in a controller, and this
 * directly places a view onto the screen.
 * 
 * Similar in some ways to a block, where this differs is that a Controller would
 * serve an entire screen, as opposed to a small chunk of it.
 * 
 * The module has the front name startermodule as dictated by the config.xml
 * config > frontend > routers > phpgenie_startermodule > args > frontName
 * 
 * This controller has the name IndexController and pinned to the front name, you'll
 * be able to access this by visiting /startermodule/index/action_name
 */

class PHPGenie_StarterModule_IndexController extends Mage_Core_Controller_Front_Action {
    /*
     * All actions are to follow the same naming convention: nameAction()
     * The following action is accessible via: /startermodule/index/test
     * 
     * If you were to create a new page, under the name 'another' then the
     * function name would be anotherAction()
     */

    public function testAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

}
