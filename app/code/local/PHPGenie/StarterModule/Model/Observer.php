<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    PHPGenie
 * @package     PHPGenie_StarterModule
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @generator   http://www.mgt-commerce.com/kickstarter/ Mgt Kickstarter
 */

/**
 * This is an Event Observer. It's stored within the Model directory as standard,
 * but doesn't take advantage of any MVC Capabilities unless you specifically ask it to.
 * It doesn't extend any pre-made functionality as each observer event could be
 * radically different from any others.
 *
 * The events contained herein are defined in the config.xml under:
 * config > global > events > [event tag] > observers > [module_shortname]_observer
 *
 * All methods include the parameter Varien_Event_Observer and it is this that can
 * be used to grab the data that's passed to the event when it's fired from the core.
 */

class PHPGenie_StarterModule_Model_Observer
{
    /**
     * methodName is defined within the config.xml under events
     *
     * @param  Varien_Event_Observer $observer
     * @author Dan Hanly
     */
    public function methodName(Varien_Event_Observer $observer)
    {
        // get the data passed to the event with the getEvent method.
        $eventData = $observer->getEvent();
    }
}